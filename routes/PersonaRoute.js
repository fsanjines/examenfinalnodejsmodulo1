const express = require('express');
const router = express.Router();

const { PersonaController } = require('../controllers');
router.get('/personas', PersonaController.listarPersonas );
router.get('/personasID/:id', PersonaController.listarPersonaId );
router.post('/personas', PersonaController.registrarPersona);
router.delete('/personasID/:id', PersonaController.eliminarPersonaId );
router.put('/personasID/:id', PersonaController.modificarPersonaId );


module.exports = router;