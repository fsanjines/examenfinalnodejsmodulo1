const mongoose = require('mongoose');
const { Schema }= mongoose;

const PersonaSchema = new Schema({
    codigo: Number,
    nombres: String,
    apellidoPaterno: String,
    apellidoMaterno: String,
    numeroDocumento: Number
});

const Persona = mongoose.model('persona', PersonaSchema);

module.exports = Persona;



