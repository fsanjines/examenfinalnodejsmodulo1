const { app, constants } = require('../config');
const { PersonaModel }  = require('../models');
const axios = require('axios');
const _ = require('underscore');
const { isValidObjectId } = require('mongoose');


const registrarPersona = async(req, res) =>{
    const datos = req.body;
    const personaCreada = await PersonaModel.create(datos);
    res.status(201).json({
        finalizado: true,
        mensaje: 'Persona registrada correctamente',
        datos: personaCreada
    });
};

const listarPersonas = async (req, res) =>{
    const listaPersonas = await PersonaModel.find();
    console.log('----LISTANDO PERSONAS----');
    //console.log(req.nombreCompleto);
    console.log(listaPersonas);
    res.status(200).json({
        finalizado: true,
        mensaje: 'Personas listadas correctamente',
        datos: listaPersonas
    });
};

const listarPersonaId = async (req, res)=>{
    try
    {
        const { id } = req.params;
        const listaPersonas = await PersonaModel.find();
        let encontrada = false;
        _.each(listaPersonas, (persona, i) =>{
            console.log('Codigo:', persona.codigo, ' Indice:', i, ' ID:', id);
            if (parseInt(persona.codigo) === parseInt(id))
            {
                encontrada = true;
                console.log('Persona encontrada en la posición:', i)
                res.status(200).json({
                    finalizado: true,
                    mensaje: 'Persona encontrada en la posición: '+i, 
                    datos: persona
                })
                console.log(persona);
            }
        });
        if (!encontrada)
        {
            res.status(200).json({
                finalizado: true,
                mensaje: 'Persona no encontrada', 
                datos: []
            })
        }
    }
    catch (error)
    {
        console.log('Debe enviar Codigo como parametro')
        res.status(406).json({
            finalizado: false,
            mensaje: 'Debe enviar Codigo',
            datos: []
        });
    };
    
};

const modificarPersonaId = async(req, res) => {
    try {
        const { id } = req.params;
        const datos = req.body;
        const listaPersonas = await PersonaModel.find();
        let encontrado = false;
        _.each(listaPersonas, async (persona, i) =>{
            if (parseInt(persona.codigo) === parseInt(id))
            {
                encontrado = true;
                console.log('Persona encontrada en la posición:', i)
                //MODIFICAR
                const personaActualizada = await PersonaModel.updateOne({ "_id" : persona._id }, datos);
                res.status(200).json({
                    finalizado: true,
                    mensaje: 'Persona actualizada en la posición: '+i, 
                    datos: datos
                })
            }
        });
        if (!encontrado)
        {
            res.status(200).json({
                finalizado: true,
                mensaje: 'Persona no encontrada', 
                datos: []
            })
        }
    } catch (error) {
        console.log('Error en eliminacion')
        res.status(406).json({
            finalizado: false,
            mensaje: 'Debe enviar Codigo',
            datos: []
        });
    }
};

const eliminarPersonaId = async (req, res) =>{
    try {
        const { id } = req.params;
        //console.log('Eliminar ', id);
        const listaPersonas = await PersonaModel.find();
        let encontrada = false;
        _.each(listaPersonas, async (persona, i) =>{
            //console.log('Codigo:', persona.codigo, ' Indice:', i, ' ID:', id);
            if (parseInt(persona.codigo) === parseInt(id))
            {
                encontrada = true;
                console.log('Persona encontrada en la posición:', i)
                //ELIMINAR
                const personaEliminada = await PersonaModel.deleteOne({ "_id" : persona._id });
                console.log(persona.codigo);
                res.status(200).json({
                    finalizado: true,
                    mensaje: 'Persona eliminada de la posición: '+i, 
                    datos: persona
                })
                console.log(persona);
            }
        });
        if (!encontrada)
        {
            res.status(200).json({
                finalizado: true,
                mensaje: 'Persona no encontrada', 
                datos: []
            })
        }
    } catch (error) {
        {
            console.log('Debe enviar Codigo como parametro')
            res.status(406).json({
                finalizado: false,
                mensaje: 'Debe enviar Codigo',
                datos: []
            });
        };  
    }
};


module.exports = {
    listarPersonas,
    listarPersonaId,
    registrarPersona,
    eliminarPersonaId,
    modificarPersonaId  
};